﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Property Name="varPersistentID:{5A406796-0205-4E0C-8209-180F8E8668A3}" Type="Ref">/My Computer/subvi/shared variables uEye.lvlib/.NET uEye shared</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="subvi" Type="Folder">
			<Item Name="bit data to array.vi" Type="VI" URL="../subvi/bit data to array.vi"/>
			<Item Name="Camera stop.vi" Type="VI" URL="../subvi/Camera stop.vi"/>
			<Item Name="ColorMode.vi" Type="VI" URL="../subvi/ColorMode.vi"/>
			<Item Name="shared variables uEye.lvlib" Type="Library" URL="../definitions/shared variables uEye.lvlib"/>
		</Item>
		<Item Name="Hallway microscope 0.9" Type="VI" URL="../Hallway microscope 0.9"/>
		<Item Name=".NET uEye.ctl" Type="VI" URL="../definitions/.NET uEye.ctl"/>
		<Item Name=".NET yEye.vi" Type="VI" URL="../definitions/.NET yEye.vi"/>
		<Item Name="adjust frame rate.vi" Type="VI" URL="../adjust frame rate.vi"/>
		<Item Name="Copy buffer to memory.vi" Type="VI" URL="../subvi/Copy buffer to memory.vi"/>
		<Item Name="Global .NET reference.vi" Type="VI" URL="../subvi/Global .NET reference.vi"/>
		<Item Name="Global test.vi" Type="VI" URL="../definitions/Global test.vi"/>
		<Item Name="load binary file.vi" Type="VI" URL="../load binary file.vi"/>
		<Item Name="queue add and configure.vi" Type="VI" URL="../queue add and configure.vi"/>
		<Item Name="setup acquisition.vi" Type="VI" URL="../setup acquisition.vi"/>
		<Item Name="simple acquisition - disp image as array.vi" Type="VI" URL="../simple acquisition - disp image as array.vi"/>
		<Item Name="simple acquisition - read 12 bit data - fps test.vi" Type="VI" URL="../simple acquisition - read 12 bit data - fps test.vi"/>
		<Item Name="simple acquisition - read 12 bit data.vi" Type="VI" URL="../simple acquisition - read 12 bit data.vi"/>
		<Item Name="simple acquisition - to queue.vi" Type="VI" URL="../simple acquisition - to queue.vi"/>
		<Item Name="simple acquisition - writting binnary data.vi" Type="VI" URL="../simple acquisition - writting binnary data.vi"/>
		<Item Name="simple display.vi" Type="VI" URL="../simple display.vi"/>
		<Item Name="simple queue add.vi" Type="VI" URL="../simple queue add.vi"/>
		<Item Name="simples acquisition.vi" Type="VI" URL="../simples acquisition.vi"/>
		<Item Name="testing buffering.vi" Type="VI" URL="../testing buffering.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="ArrayToImage.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Library/ArrayToImage.vi"/>
				<Item Name="Error_Handling.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Library/Error_Handling.vi"/>
				<Item Name="SubVI_Example_CameraInit.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Examples/Example Modules SubVIs/SubVI_Example_CameraInit.vi"/>
				<Item Name="SubVI_Example_GetDefault_Timing.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Examples/Example Modules SubVIs/SubVI_Example_GetDefault_Timing.vi"/>
				<Item Name="SubVI_Example_GetExposure.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Examples/Example Modules SubVIs/SubVI_Example_GetExposure.vi"/>
				<Item Name="SubVI_Example_GetFramerate.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Examples/Example Modules SubVIs/SubVI_Example_GetFramerate.vi"/>
				<Item Name="SubVI_Example_GetImageSize.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Examples/Example Modules SubVIs/SubVI_Example_GetImageSize.vi"/>
				<Item Name="SubVI_Example_GetPixelClock.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Examples/Example Modules SubVIs/SubVI_Example_GetPixelClock.vi"/>
				<Item Name="SubVI_Example_Init_Camera_Error_Handling.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Examples/Example Modules SubVIs/SubVI_Example_Init_Camera_Error_Handling.vi"/>
				<Item Name="SubVI_Example_Is_XS.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Examples/Example Modules SubVIs/SubVI_Example_Is_XS.vi"/>
				<Item Name="SubVI_Example_MemoryAllocationSequence.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Examples/Example Modules SubVIs/SubVI_Example_MemoryAllocationSequence.vi"/>
				<Item Name="SubVI_Example_StartLive.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Examples/Example Modules SubVIs/SubVI_Example_StartLive.vi"/>
				<Item Name="SubVI_Example_SyncEventLoop.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Examples/Example Modules SubVIs/SubVI_Example_SyncEventLoop.vi"/>
				<Item Name="SubVI_Example_Timing.vi" Type="VI" URL="../../../../../../Program Files/National Instruments/LabVIEW 2017/instr.lib/IDS/ueye.NET/Examples/Example Modules SubVIs/SubVI_Example_Timing.vi"/>
				<Item Name="uEyeDotNet.dll" Type="Document" URL="/&lt;instrlib&gt;/IDS/ueye.NET/uEyeDotNet.dll"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Draw 1-Bit Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw 1-Bit Pixmap.vi"/>
				<Item Name="Draw 4-Bit Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw 4-Bit Pixmap.vi"/>
				<Item Name="Draw 8-Bit Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw 8-Bit Pixmap.vi"/>
				<Item Name="Draw Flattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Flattened Pixmap.vi"/>
				<Item Name="Draw True-Color Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw True-Color Pixmap.vi"/>
				<Item Name="Draw Unflattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Unflattened Pixmap.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="FixBadRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/FixBadRect.vi"/>
				<Item Name="Flatten Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pixmap.llb/Flatten Pixmap.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="LVCursorListTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVCursorListTypeDef.ctl"/>
			</Item>
			<Item Name="free memory.vi" Type="VI" URL="../subvi/free memory.vi"/>
			<Item Name="start acquisition.vi" Type="VI" URL="../subvi/start acquisition.vi"/>
			<Item Name="stop acquisition.vi" Type="VI" URL="../subvi/stop acquisition.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="SubVI_Example_MemoryAllocation_RadekMod.vi" Type="VI" URL="../subvi/SubVI_Example_MemoryAllocation_RadekMod.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
