﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Low level Tiff conversion" Type="Folder">
			<Item Name="CTIFF Writing Example.vi" Type="VI" URL="../../../ctiff/CTIFF Writing Example.vi"/>
			<Item Name="ctiff_stack_addpage_x64.vi" Type="VI" URL="../../../ctiff/ctiff_stack_addpage_x64.vi"/>
		</Item>
		<Item Name="single folder with spool to tiff.vi" Type="VI" URL="../../single folder with spool to tiff.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
			</Item>
			<Item Name="__CTIFFAddNewPageFLOAT32.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFAddNewPageFLOAT32.vi"/>
			<Item Name="__CTIFFAddNewPageFLOAT64.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFAddNewPageFLOAT64.vi"/>
			<Item Name="__CTIFFAddNewPageINT8.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFAddNewPageINT8.vi"/>
			<Item Name="__CTIFFAddNewPageINT16.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFAddNewPageINT16.vi"/>
			<Item Name="__CTIFFAddNewPageINT32.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFAddNewPageINT32.vi"/>
			<Item Name="__CTIFFAddNewPageUINT8.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFAddNewPageUINT8.vi"/>
			<Item Name="__CTIFFAddNewPageUINT16.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFAddNewPageUINT16.vi"/>
			<Item Name="__CTIFFAddNewPageUINT32.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFAddNewPageUINT32.vi"/>
			<Item Name="__CTIFFCloseFile.vi" Type="VI" URL="../../../../ctiff/ctiffx64.llb/__CTIFFCloseFile.vi"/>
			<Item Name="__CTIFFNewFileAndStyleFLOAT32.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFNewFileAndStyleFLOAT32.vi"/>
			<Item Name="__CTIFFNewFileAndStyleFLOAT64.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFNewFileAndStyleFLOAT64.vi"/>
			<Item Name="__CTIFFNewFileAndStyleINT8.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFNewFileAndStyleINT8.vi"/>
			<Item Name="__CTIFFNewFileAndStyleINT16.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFNewFileAndStyleINT16.vi"/>
			<Item Name="__CTIFFNewFileAndStyleINT32.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFNewFileAndStyleINT32.vi"/>
			<Item Name="__CTIFFNewFileAndStyleUINT8.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFNewFileAndStyleUINT8.vi"/>
			<Item Name="__CTIFFNewFileAndStyleUINT16.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFNewFileAndStyleUINT16.vi"/>
			<Item Name="__CTIFFNewFileAndStyleUINT32.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFNewFileAndStyleUINT32.vi"/>
			<Item Name="__CTIFFPixelTypeFLOAT32.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFPixelTypeFLOAT32.vi"/>
			<Item Name="__CTIFFPixelTypeFLOAT64.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFPixelTypeFLOAT64.vi"/>
			<Item Name="__CTIFFPixelTypeINT8.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFPixelTypeINT8.vi"/>
			<Item Name="__CTIFFPixelTypeINT16.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFPixelTypeINT16.vi"/>
			<Item Name="__CTIFFPixelTypeINT32.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFPixelTypeINT32.vi"/>
			<Item Name="__CTIFFPixelTypeUINT8.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFPixelTypeUINT8.vi"/>
			<Item Name="__CTIFFPixelTypeUINT16.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFPixelTypeUINT16.vi"/>
			<Item Name="__CTIFFPixelTypeUINT32.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/__CTIFFPixelTypeUINT32.vi"/>
			<Item Name="ctiff_stack_close_x64.vi" Type="VI" URL="../../../../ctiff/ctiff_stack_close_x64.vi"/>
			<Item Name="ctiff_stack_open_x64.vi" Type="VI" URL="../../../ctiff/ctiff_stack_open_x64.vi"/>
			<Item Name="CTIFFAddNewPage.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/CTIFFAddNewPage.vi"/>
			<Item Name="CTIFFCloseFile.vi" Type="VI" URL="../../../../ctiff/ctiffx64.llb/CTIFFCloseFile.vi"/>
			<Item Name="CTIFFError.vi" Type="VI" URL="../../../../ctiff/ctiffx64.llb/CTIFFError.vi"/>
			<Item Name="CTIFFGetPixelType.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/CTIFFGetPixelType.vi"/>
			<Item Name="CTIFFNewFile.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/CTIFFNewFile.vi"/>
			<Item Name="CTIFFNewFileAndStyle.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/CTIFFNewFileAndStyle.vi"/>
			<Item Name="CTIFFSetBasicMeta.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/CTIFFSetBasicMeta.vi"/>
			<Item Name="CTIFFSetPageStyle.vi" Type="VI" URL="../../../ctiff/ctiffx64.llb/CTIFFSetPageStyle.vi"/>
			<Item Name="CTIFFWriteAndClose.vi" Type="VI" URL="../../../../ctiff/ctiffx64.llb/CTIFFWriteAndClose.vi"/>
			<Item Name="CTIFFWriteFile.vi" Type="VI" URL="../../../../ctiff/ctiffx64.llb/CTIFFWriteFile.vi"/>
			<Item Name="libctiff64.dll" Type="Document" URL="../../../../ctiff/libctiff64.dll"/>
			<Item Name="Spool file to 3D array.vi" Type="VI" URL="../../Spool file to 3D array.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
